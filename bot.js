require('dotenv').config()
const Discord = require('discord.js')
const client = new Discord.Client()

client.on('ready', () => {
    console.log(`Logged in as ${client.user.tag}`)
})

client.on('message', async message => {
    let words = message.content.split(' ')

    let modRole = await message.guild.roles.find('name', 'mod')
    await console.log(modRole)

    if(words[0] === '!gamertag') {
        message.reply(words[1])
    }
})

client.login(process.env.TOKEN)